name = "josé alves"
print(name.title()) # transforma o texto em capitular.
print(name.upper()) # transgorma o texto em letras maiúsculas.
print(name.lower()) # transforma o texto em letras minúsculas.

first_name = "josé"
last_name = "alves"
full_name = first_name + " " + last_name
print(full_name)

message = "Olá! " + full_name.title() + "."
print(message)